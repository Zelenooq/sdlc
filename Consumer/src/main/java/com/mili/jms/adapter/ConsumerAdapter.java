package com.mili.jms.adapter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.mili.jms.listener.ConsumerListener;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.util.JSON;

@Component
public class ConsumerAdapter {

	private static Logger logger = LogManager.getLogger(ConsumerListener.class.getName());


	public void sendToMongo(String json) {
		logger.info("Sending to MongoDB");
		MongoClient client=new MongoClient();
		DB db=client.getDB("vendor");
		DBCollection collection=db.getCollection("contact");
		logger.info("Converting JSON to DBObject:"+collection);
		BasicDBObject dbObj=BasicDBObject.parse(json);
		logger.info("JSON is converted");
		//(DBOject)JSON.parse(json);
		collection.insert(dbObj);
		logger.info("Sent to MongoDB");
	}

}
